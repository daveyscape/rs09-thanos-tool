import org.json.simple.JSONArray
import org.json.simple.JSONObject
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.lang.StringBuilder
import javax.script.ScriptEngineManager

object TableData {
    class Shop(var id: Int,var title: String,var stock: ArrayList<Item>,var npcs: String,var currency: Int,var general_store: Boolean,var high_alch: Boolean,var forceShared: Boolean)
    val tables = ArrayList<NPCDropTable>()
    val itemNames = hashMapOf<Int,String>()
    val npcNames = hashMapOf<Int,String>()
    val npcConfigKeys = HashSet<String>()
    val npcConfigs = ArrayList<JSONObject>()
    val objConfigKeys = HashSet<String>()
    val objConfigs = ArrayList<JSONObject>()
    var itemConfigKeys = HashSet<String>()
    var itemConfigs = ArrayList<JSONObject>()
    val shops = hashMapOf<Int,Shop>()

    fun getItemName(id: Int): String{
        return when(id){
            31 -> "RDT Slot"
            1 -> "Clue Scroll (easy)"
            5733 -> "Clue Scroll (med)"
            12070 -> "Clue Scroll (hard)"
            0 -> "Nothing"
            else -> itemNames[id] ?: "Unknown"
        }
    }
}

